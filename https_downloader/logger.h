#ifndef LOGGER_INIT
#define LOGGER_INIT
#include <string>
#define USE_LOGGER false

void logInit();

void writeLogMsgInfo(const std::string &msg);
void writeLogError(const std::string &msg);
void writeLogMsgWarning(const std::string &msg);

#endif // LOGGER_INIT

