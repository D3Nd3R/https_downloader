#include "https_downloader.h"
#include "logger.h"

#include <QDebug>
#include <QAuthenticator>
#include <QByteArray>
#include <QNetworkRequest>
#include <QSslConfiguration>
#include <QVariant>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QDir>
#include <QApplication>
#include <QEventLoop>
#include <QList>
#include <QPair>

#include <exception>

HttpsDownloader::HttpsDownloader()
{
    mpNetManager = new QNetworkAccessManager();
    if (mpNetManager == nullptr)
        qDebug() << "can't create QNetworkAccessManager";
    setConnects();
}

HttpsDownloader::HttpsDownloader(const QString &user, const QString &pass,
                                 const QString &host, const QString &request,
                                 const QString &pathToSave) :
    mUser(user), mPass(pass),mHost(host) ,mRequest(request), mPathToSave(pathToSave)
{
    mpNetManager = new QNetworkAccessManager();
    if (mpNetManager == nullptr)
        qDebug() << "can't create QNetworkAccessManager";
    setConnects();

    if (mPathToSave == "-1")
        mPathToSave = QDir().currentPath();

    if (*(mPathToSave.end() - 1) != '/')
        mPathToSave += '/';

    if (!QDir().exists(mPathToSave))
        if (!QDir().mkdir(mPathToSave))
            throw std::runtime_error("Programm can't create path to save!!!");

}

HttpsDownloader::~HttpsDownloader()
{
    if (mpNetManager)
        delete mpNetManager;
    writeLogMsgInfo("~HttpsDownloader()");
}

void HttpsDownloader::setUser(const QString &user)
{
    mUser = user;
}

void HttpsDownloader::setPassword(const QString &pass)
{
    mPass = pass;
}

QString HttpsDownloader::getUser() const
{
    return mUser;
}

QString HttpsDownloader::getPassword() const
{
    return mPass;
}

void HttpsDownloader::downloadFile(const QString &camId,
                                   const QString &preLastIndex)
{
#ifdef QT_DEBUG
    qDebug() <<"-------------------downloadFile---------------";
#endif

    QEventLoop localEventLoop;
    mCamId = camId;
    QUrl url(mHost + mRequest.arg(camId,preLastIndex));

    QNetworkRequest req = QNetworkRequest(url);

    QSslConfiguration conf = req.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    req.setSslConfiguration(conf);

    connect(mpNetManager->get(req), SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(onReplyError(QNetworkReply::NetworkError)));
    connect(mpNetManager, &QNetworkAccessManager::finished,
            this, &HttpsDownloader::onFinished);
    connect(mpNetManager, &QNetworkAccessManager::finished,
            [this, &localEventLoop](QNetworkReply *reply)
    {
        localEventLoop.exit();
    });

    localEventLoop.exec();
}

void HttpsDownloader::shutDown()
{
    if (mpNetManager)
        delete mpNetManager;

    if (mFile)
        if (mFile->isOpen())
        {
            mFile->close();
            delete mFile;
        }
    emit emitShutDown();
}


void HttpsDownloader::setConnects()
{
    connect(mpNetManager, &QNetworkAccessManager::authenticationRequired,
            this, &HttpsDownloader::onAuthenticationRequired);
}

void HttpsDownloader::onAuthenticationRequired(QNetworkReply *,
                                               QAuthenticator *authenticator)
{
    authenticator->setUser(mUser);
    authenticator->setPassword(mPass);
}

void HttpsDownloader::sslErrors(QNetworkReply *, const QList<QSslError> &errors)
{
    QString errorString;
    foreach (const QSslError &error, errors) {
        if (!errorString.isEmpty())
            errorString += '\n';
        errorString += error.errorString();
    }
    qDebug() << errorString;
}

void HttpsDownloader::onReplyError(QNetworkReply::NetworkError code)
{
    qDebug() << code;
}

void HttpsDownloader::onFinished(QNetworkReply *reply)
{
    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    disconnect(mpNetManager,&QNetworkAccessManager::finished,
               this,&HttpsDownloader::onFinished);
    if (statusCode.toInt() == 200)
    {

#ifdef QT_DEBUG
        qDebug() << reply->attribute(
                        QNetworkRequest::HttpReasonPhraseAttribute).toString();
        auto headerPairs =  reply->rawHeaderPairs();
       foreach (auto& headerPair, headerPairs) {
            qDebug() << headerPair.first << "   " << headerPair.second;
        }
#endif
        QJsonDocument result = QJsonDocument::fromJson(reply->readAll());
        QJsonObject rootObj = result.object();
        //if (rootObj.empty())
          //  return;
        qDebug() << rootObj.keys().at(1) << "   "
                 << rootObj.value(rootObj.keys().at(1)).toString();

        QString fName(rootObj.value(rootObj.keys().at(1)).toString());

        QString binImg = rootObj.value(rootObj.keys().at(0)).toString();

        QString fullPathToSavedFile;
        fullPathToSavedFile = mPathToSave + mCamId + "/";// + fName;
        if (!QDir().exists(fullPathToSavedFile))
            if (!QDir().mkdir(fullPathToSavedFile))
                throw std::runtime_error("Programm can't create path to save!!!");
        fullPathToSavedFile += fName;


        if (!mFile)
        {
            mFile = new QFile(fullPathToSavedFile);
        }

        mFile->open(QIODevice::WriteOnly);


        if (mFile->isOpen())
        {
            QByteArray arr;
            arr.append(binImg);

            mFile->write(QByteArray::fromBase64(arr));
            mFile->close();
        }

        qDebug() << "onFinished";
        if(mFile)
        {
            delete mFile;
            mFile = Q_NULLPTR;
        }

        emit hasImageDownloded(true, fullPathToSavedFile);
    }
    else
    {
        qDebug() << "status code: " << statusCode.toString();
        qDebug() << reply->attribute(
                        QNetworkRequest::HttpReasonPhraseAttribute).toString();
        emit hasImageDownloded(false, "null");
    }
#ifdef QT_DEBUG
    qDebug() << "//===========================================================//";
#endif
}

void HttpsDownloader::onDownloadFile(const QString &camId, const QString &preLastIndex)
{
    QString logMsg;
    logMsg = "Download file: camera id: %1, preLastIndex: %2";
    logMsg.arg(camId,preLastIndex);
    writeLogMsgInfo("download");

    this->downloadFile(camId, preLastIndex);
}

