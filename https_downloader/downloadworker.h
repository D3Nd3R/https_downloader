#ifndef DOWNLOADWORKER_H
#define DOWNLOADWORKER_H

#include <QString>
#include <QNetworkAccessManager>
#include <QObject>
#include <QUrl>
#include <QFile>
#include <QList>
#include <QSslError>
#include <QNetworkReply>
#include <QRunnable>

#include <opencv2/core/core.hpp>

class DownloadWorker : public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit DownloadWorker(QObject *parent = 0);
    explicit DownloadWorker(const QString &user, const QString &pass,
                            const QString &host, const QString &request,
                            const QString &camId,
                            const QString &fileName,
                            const QString &pathToSave = "-1",
                            QObject *parent = 0);
    ~DownloadWorker();

    void run() override;

private:
    void setConnects();
    void downloadFile(const QString &camId, const QString &preLastIndex = "");

    cv::Mat byteArrToMatConverter(const QByteArray &inByteArr);


private slots:
    void onAuthenticationRequired(QNetworkReply*,
                                  QAuthenticator *authenticator);
    void sslErrors(QNetworkReply*,const QList<QSslError> &errors);
    void onReplyError(QNetworkReply::NetworkError code);
    void onFinished(QNetworkReply *reply);

signals:
    void hasImageDownloded(bool isDownloded, QString pathToImg);
    void downloadedImg(const QString &camId, const QString &imgIndex);
    void emitShutDown();
    void done();
    void newFrame(const QString &camId,const QString &frameName, cv::Mat frame);

private:
    QString mUser;
    QString mPass;
    QString mHost;
    QString mRequest;
    QString mPathToSave;
    QString mCamId;
    QString mFileName;

    QNetworkAccessManager *mpNetManager;

    QFile *mFile;
};

#endif // DOWNLOADWORKER_H
