#include "https_downloader.h"
#include "downloadworker.h"
#include "logger.h"
#include "downloadmanager.h"


#include <QCoreApplication>
#include <QDir>
#include <QApplication>
#include <QThread>
#include <QThreadPool>
#include <utility>
#include <QMap>

#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#ifdef Q_OS_LINUX
#include <unistd.h>
#endif

//#define DAEMON


QString pathToSave = "/media/d3nd3r/5CB93519D3437541/picsFromCameras";

int main(int argc, char *argv[])
{
    logInit();
    writeLogMsgInfo("app start");

    if (argc != 2)
    {
        std::cerr << "Specify path to save file!!!" << std::endl;
        writeLogError("Specify path to save file!!!");
        return -1;
    }

    pathToSave = argv[1];
    qDebug() << argv[1];

    QCoreApplication a(argc, argv);
    QDir::setCurrent(qApp->applicationDirPath());



#ifdef Q_OS_LINUX
#ifdef DAEMON
    daemon(0,0);
#endif
#endif

    QString camIdBase("90100000%1");
    QMap<QString, QString> cameras1;
    /*for (int i = 7; i < 10; ++i)
    {
        cameras1[camIdBase.arg(i)] = "";
    }*/
    cameras1["901000007"] = "";
    cameras1["901000039"] = "";
    cameras1["901000025"] = "";

    DownloadManager *dMan1
            =new DownloadManager(
                                 "Analitic", "GPanaliticserv",
                                 "https://analitics.getprk.ru",
                                 "/get_last_picture?id_cam=%1&pre_last_picture_index=%2",
                                 pathToSave);



  /*
    qDebug() << dMan1->getAviableCams();
    QObject::connect(dMan1, &DownloadManager::newFrameFromCamera,[](const QString &camId, cv::Mat frame)
    {
        cv::imshow(camId.toStdString(), frame);
        cv::waitKey();

    });
*/
    dMan1->download();
    dMan1->downloadByTimer(1000);

    return a.exec();
}

