#include "downloadworker.h"
#include "logger.h"

#include <QDebug>
#include <QAuthenticator>
#include <QByteArray>
#include <QNetworkRequest>
#include <QSslConfiguration>
#include <QVariant>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QDir>
#include <QApplication>
#include <QEventLoop>
#include <QList>
#include <QPair>

#include <QThread>

#include <exception>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


DownloadWorker::DownloadWorker(QObject *parent) : QObject(parent)
{

}

DownloadWorker::DownloadWorker(const QString &user, const QString &pass,
                               const QString &host, const QString &request,
                               const QString &camId, const QString &fileName,
                               const QString &pathToSave/* = "-1"*/,
                               QObject *parent/* = 0*/) :
    QObject(parent), mUser(user), mPass(pass),mHost(host),
    mRequest(request),mCamId(camId), mFileName(fileName),
    mPathToSave(pathToSave)
{
    mFile = Q_NULLPTR;
}

DownloadWorker::~DownloadWorker()
{
    if (mpNetManager)
        delete mpNetManager;
    if (mFile)
        delete mFile;

    QString logMsg;
    logMsg = "~DownloadWorker(): camera id: %1";
    logMsg = logMsg.arg(mCamId);
    writeLogMsgInfo(logMsg.toStdString());
}

void DownloadWorker::run()
{

    mpNetManager = new QNetworkAccessManager();
    if (mpNetManager == nullptr)
    {   writeLogError("Can't create QNetworkAccessManager");
        throw std::runtime_error("can't create QNetworkAccessManager");
    }
    setConnects();

    if (mPathToSave == "-1")
        mPathToSave = QDir().currentPath();

    if (*(mPathToSave.end() - 1) != '/')
        mPathToSave += '/';

    if (!QDir().exists(mPathToSave))
        if (!QDir().mkdir(mPathToSave))
        {
            writeLogError("Programm can't create path to save!!!");
            throw std::runtime_error("Programm can't create path to save!!!");
        }

    this->downloadFile(mCamId,mFileName);
}

void DownloadWorker::setConnects()
{
    connect(mpNetManager, &QNetworkAccessManager::authenticationRequired,
            this, &DownloadWorker::onAuthenticationRequired,Qt::QueuedConnection);
}

void DownloadWorker::downloadFile(const QString &camId, const QString &preLastIndex)
{
#ifdef QT_DEBUG
    qDebug() <<"-------------------downloadFile---------------";
#endif
    QString logMsg;
    logMsg = "Download file: camera id: %1, preLastIndex: %2";
    logMsg = logMsg.arg(camId).arg(preLastIndex);
    writeLogMsgInfo(logMsg.toStdString());

    QEventLoop localEventLoop;
    mCamId = camId;
    QUrl url(mHost + mRequest.arg(camId,preLastIndex));
    url.setUserName(mUser);
    url.setPassword(mPass);

    QNetworkRequest req = QNetworkRequest(url);

    QSslConfiguration conf = req.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    req.setSslConfiguration(conf);

    connect(mpNetManager->get(req), SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(onReplyError(QNetworkReply::NetworkError)),Qt::QueuedConnection);
    connect(mpNetManager, &QNetworkAccessManager::finished,
            this, &DownloadWorker::onFinished,Qt::QueuedConnection);

    connect(this, &DownloadWorker::done,
            [this, &localEventLoop]()
    {
        localEventLoop.exit();
    });

    localEventLoop.exec();
}

cv::Mat DownloadWorker::byteArrToMatConverter(const QByteArray &inByteArr)
{
    std::vector<unsigned char> buf(inByteArr.begin(), inByteArr.end());
    return cv::imdecode(buf,cv::IMREAD_COLOR);
}

void DownloadWorker::onAuthenticationRequired(QNetworkReply *, QAuthenticator *authenticator)
{
    authenticator->setUser(mUser);
    authenticator->setPassword(mPass);
    writeLogMsgInfo("onAuthenticationRequired");
}

void DownloadWorker::sslErrors(QNetworkReply *, const QList<QSslError> &errors)
{
    QString errorString;
    foreach (const QSslError &error, errors) {
        if (!errorString.isEmpty())
            errorString += '\n';
        errorString += error.errorString();
        writeLogError(errorString.toStdString());
    }
    qDebug() << errorString;
}

void DownloadWorker::onReplyError(QNetworkReply::NetworkError code)
{
    qDebug() << code;
}

void DownloadWorker::onFinished(QNetworkReply *reply)
{
    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    disconnect(mpNetManager,&QNetworkAccessManager::finished,
               this,&DownloadWorker::onFinished);
    if (statusCode.toInt() == 200)
    {

#ifdef qt//QT_DEBUG
        qDebug() << reply->attribute(
                        QNetworkRequest::HttpReasonPhraseAttribute).toString();
        auto headerPairs =  reply->rawHeaderPairs();
        foreach (auto& headerPair, headerPairs) {
            qDebug() << headerPair.first << "   " << headerPair.second;
        }
#endif
        QJsonDocument result = QJsonDocument::fromJson(reply->readAll());
        QJsonObject rootObj = result.object();
        if (rootObj.empty())
            return;
#ifdef qw//QT_DEBUG
        qDebug() << rootObj.keys().at(1) << "   "
                 << rootObj.value(rootObj.keys().at(1)).toString();
#endif

        QString fName(rootObj["image_name"].toString());

        QString binImg = rootObj["image"].toString();

        QString fullPathToSavedFile;
        fullPathToSavedFile = mPathToSave + mCamId + "/";// + fName;
        if (!QDir().exists(fullPathToSavedFile))
            if (!QDir().mkdir(fullPathToSavedFile))
            {   writeLogError("Programm can't create path to save!!!");
                throw std::runtime_error("Programm can't create path to save!!!");
            }
        fullPathToSavedFile += fName;
#ifdef QT_DEBUG
        qDebug() << fullPathToSavedFile;
#endif

        if (!mFile)
        {
            mFile = new QFile(fullPathToSavedFile);
        }

        mFile->open(QIODevice::WriteOnly);

        if (mFile->isOpen())
        {
            mFile->write(QByteArray::fromBase64(binImg.toLatin1()));
            mFile->close();
        }
#ifdef QT_DEBUG
        qDebug() << "onFinished";
#endif
        if(mFile)
        {
            delete mFile;
            mFile = Q_NULLPTR;
        }

        QString logMsg;
        logMsg = "Image %1 from camera %2 has downloaded";
        logMsg = logMsg.arg(fName).arg(mCamId);
        writeLogMsgInfo(logMsg.toStdString());

        cv::Mat recImg =
                byteArrToMatConverter(QByteArray::fromBase64(binImg.toLatin1()));

        emit newFrame(mCamId, fName, recImg);
        emit downloadedImg(mCamId, fName);
        emit hasImageDownloded(true, fullPathToSavedFile);
    }
    else
    {
#ifdef QT_DEBUG
        qDebug() << "status code: " << statusCode.toString();
        qDebug() << reply->attribute(
                        QNetworkRequest::HttpReasonPhraseAttribute).toString();
#endif
        QString logMsg;
        logMsg = "Reply error %1 %2";
        logMsg = logMsg.arg(
                    statusCode.toString()).arg(
                    reply->attribute(
                        QNetworkRequest::HttpReasonPhraseAttribute).toString());
        writeLogMsgInfo(logMsg.toStdString());

        emit hasImageDownloded(false, "null");
    }
    delete reply;
    emit done();
#ifdef QT_DEBUG
    qDebug() << "//===========================================================//";
#endif
}

