QT += core network widgets
QT -= gui

TARGET = https_downloader
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++14

TEMPLATE = app

INCLUDEPATH += /usr/local/include/opencv2/
INCLUDEPATH += /usr/local/include/boost
LIBS += -L/usr/local/lib/ -lopencv_core -lopencv_highgui -lopencv_imgcodecs

SOURCES += main.cpp \
    https_downloader.cpp \
    downloadworker.cpp \
    logger.cpp \
    downloadmanager.cpp

HEADERS += \
    https_downloader.h \
    downloadworker.h \
    logger.h \
    downloadmanager.h

DEFINES += BOOST_LOG_DYN_LINK

LIBS += -lboost_log -lboost_system -lboost_thread -lboost_log_setup



