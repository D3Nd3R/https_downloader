#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QThreadPool>
#include <QEventLoop>
#include <QVector>
#include <QString>
#include <QNetworkReply>

#include <opencv2/core/core.hpp>


class QTimer;
class QAuthenticator;

class DownloadManager : public QObject
{
    Q_OBJECT
public:
    explicit DownloadManager(QObject *parent = 0);
    explicit DownloadManager(const QMap<QString, QString> &listOfCameras,
                             const QString &user, const QString &pass,
                             const QString &host, const QString &request,
                             const QString &pathToSave = "-1",
                             QObject *parent = 0);
    explicit DownloadManager(const QString &user, const QString &pass,
                             const QString &host, const QString &request,
                             const QString &pathToSave = "-1",
                             QObject *parent = 0);
    ~DownloadManager();

    void download();
    void downloadByTimer(const int msec);
    void stopTimer();

    QVector<QString> getAviableCams();

signals:
    void newFrameFromCamera(const QString &camId, const QString &frameName, cv::Mat frame);
    void hasCamsListDownload();
private slots:
    void onFileDownloaded(const QString &camId, const QString &imgId);
    void onFrameDownloaded(const QString &camId,const QString &frameName, cv::Mat frame);

private slots: //For getAviableCams
   void onRecvCams(QNetworkReply *reply);
   void onStopLocalEventLoop();
   void onAuthentication(QNetworkReply *, QAuthenticator *authenticator);
   void onReplyError(QNetworkReply::NetworkError code);

private:
   QMap<QString, QString> prepareCamListForReq();

private:
    QString mUser;
    QString mPass;
    QString mHost;
    QString mRequest;
    QString mPathToSave;

    QMap<QString, QString> mCameraAndLastImgId;
    QTimer *mpTimer;
    QThreadPool *mpThreadPool;

    bool mIsUseCurrentCamList;

private:
    QEventLoop *pLocalEventLoop;
    QVector<QString> cams;
};

#endif // DOWNLOADMANAGER_H
