#include "downloadmanager.h"
#include "downloadworker.h"
#include "logger.h"

#include <utility>
#include <QTimer>
#include <QDebug>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QByteArray>
#include <QAuthenticator>

#include <opencv2/highgui/highgui.hpp>

#include <functional>
#include <utility>

DownloadManager::DownloadManager(QObject *parent) : QObject(parent)
{
    mpTimer = Q_NULLPTR;
    mpThreadPool = QThreadPool::globalInstance();
    mIsUseCurrentCamList = false;
}

DownloadManager::DownloadManager(const QMap<QString, QString> &listOfCameras,
                                 const QString &user, const QString &pass,
                                 const QString &host, const QString &request,
                                 const QString &pathToSave, QObject *parent) :
    QObject(parent), mUser(user), mPass(pass),mHost(host),
    mRequest(request), mPathToSave(pathToSave),
    mCameraAndLastImgId(listOfCameras)
{
    mpTimer = Q_NULLPTR;
    mpThreadPool = QThreadPool::globalInstance();
    mIsUseCurrentCamList = true;
}

DownloadManager::DownloadManager(const QString &user, const QString &pass,
                                 const QString &host, const QString &request,
                                 const QString &pathToSave, QObject *parent) :
    QObject(parent), mUser(user), mPass(pass),mHost(host),
    mRequest(request), mPathToSave(pathToSave)
{
    mpTimer = Q_NULLPTR;
    mpThreadPool = QThreadPool::globalInstance();
    mIsUseCurrentCamList = false;
}

DownloadManager::~DownloadManager()
{
    if(mpTimer)
        delete mpTimer;
}

void DownloadManager::download()
{

    auto camsForCurReq = this->prepareCamListForReq();
    if (camsForCurReq.isEmpty()) {
        qDebug() << "no cams aviable";
        writeLogMsgWarning("all cams are not aviable");
    }

    if (!mpThreadPool)
    {
        writeLogError("Can't create thread pool DownloadManager::download()");
        return;
    }
    for (auto it = camsForCurReq.begin();
         it != camsForCurReq.end(); ++it)
    {
        DownloadWorker *worker =
                std::move(new DownloadWorker(
                              mUser, mPass,
                              mHost, mRequest,
                              it.key(), it.value(),
                              mPathToSave));
        connect(worker,&DownloadWorker::downloadedImg,
                this, &DownloadManager::onFileDownloaded);
        connect(worker,&DownloadWorker::newFrame,
                this, &DownloadManager::onFrameDownloaded);

        mpThreadPool->start(worker);
    }
}

void DownloadManager::downloadByTimer(const int msec)
{
    if (!mpTimer)
        mpTimer = new QTimer();

    if (!mpTimer)
    {
        writeLogError("Can't create timer DownloadManager::downloadByTimer");
        return;
    }
    mpTimer->start(msec);

    connect(mpTimer, &QTimer::timeout,
            this, &DownloadManager::download);

    QString logMsg("Start downloading by timer with time interval %1");

    writeLogMsgInfo(logMsg.arg(msec).toStdString());
}

void DownloadManager::stopTimer()
{
    if (mpTimer && mpTimer->isActive())
        mpTimer->stop();
    writeLogMsgInfo("Downloading by timer has been stopped");
}

QVector<QString> DownloadManager::getAviableCams()
{
    pLocalEventLoop = new QEventLoop();

    QUrl url(mHost + QString("/get_available_cams"));
    url.setUserName(mUser);
    url.setPassword(mPass);

    QNetworkRequest req = QNetworkRequest(url);
    QNetworkAccessManager netManager;

    QSslConfiguration conf = req.sslConfiguration();
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);
    req.setSslConfiguration(conf);

    auto reply = netManager.get(req);

     connect(&netManager, &QNetworkAccessManager::finished,
            this, &DownloadManager::onRecvCams, Qt::DirectConnection);

    connect(this, &DownloadManager::hasCamsListDownload,
            this, &DownloadManager::onStopLocalEventLoop,Qt::DirectConnection);

    connect(&netManager, &QNetworkAccessManager::authenticationRequired,
           this, &DownloadManager::onAuthentication, Qt::DirectConnection);

    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(onReplyError(QNetworkReply::NetworkError)));


    pLocalEventLoop->exec();
    delete pLocalEventLoop;
    return cams;
}

void DownloadManager::onFileDownloaded(const QString &camId, const QString &imgId)
{
    mCameraAndLastImgId[camId] = imgId;
}

void DownloadManager::onFrameDownloaded(const QString &camId,const QString &frameName, cv::Mat frame)
{
    emit newFrameFromCamera(camId, frameName, frame);
}

void DownloadManager::onRecvCams(QNetworkReply *reply)
{
    int statusCode = reply->attribute(
                QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if (statusCode == 200)
    {
        QJsonDocument result = QJsonDocument::fromJson(reply->readAll());
        QJsonObject rootObj = result.object();
        if (rootObj.empty())
            return;

        auto tmp = rootObj["cam_id"].toArray();
        cams.resize(tmp.size());

        for(size_t i = 0; i < tmp.size(); ++i)
        {
            cams[i] = QString::number(tmp[i].toInt());
        }
    }
    emit this->hasCamsListDownload();
    delete reply;
}

void DownloadManager::onStopLocalEventLoop()
{
    this->pLocalEventLoop->quit();
}

void DownloadManager::onAuthentication(QNetworkReply *, QAuthenticator *authenticator)
{
    authenticator->setUser(this->mUser);
    authenticator->setPassword(this->mPass);
}

void DownloadManager::onReplyError(QNetworkReply::NetworkError code)
{
    qDebug() << code;
}

QMap<QString, QString> DownloadManager::prepareCamListForReq()
{
    if (mIsUseCurrentCamList)
        return mCameraAndLastImgId;

    auto aviableCams = this->getAviableCams();
    QMap<QString,QString> camsForCurReq;

    foreach (auto curCamId, aviableCams)
    {
        if(mCameraAndLastImgId.contains(curCamId))
        {
            camsForCurReq[curCamId] = mCameraAndLastImgId[curCamId];
        }
        else
        {
            camsForCurReq[curCamId] = "";
            mCameraAndLastImgId[curCamId] = "";
        }
    }

    return camsForCurReq;
}

