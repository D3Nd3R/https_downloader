#ifndef HTTPS_DOWNLOADER_H
#define HTTPS_DOWNLOADER_H

#include <QString>
#include <QNetworkAccessManager>
#include <QObject>
#include <QUrl>
#include <QFile>
#include <QList>
#include <QSslError>
#include <QNetworkReply>

#include <vector>

class HttpsDownloader : public QObject
{
    Q_OBJECT
public:
    HttpsDownloader();
    HttpsDownloader(const QString &user, const QString &pass,
                    const QString &host, const QString &request,
                    const QString &pathToSave = "-1");
    ~HttpsDownloader();

    void setUser(const QString &user);
    void setPassword(const QString &pass);
    void setHost(const QString &host);

    QString getUser() const;
    QString getPassword() const;
    QString getHost() const;

    void downloadFile(const QString &camId, const QString &preLastIndex = "");
    void shutDown();

private:
    void setConnects();

private slots:
    void onAuthenticationRequired(QNetworkReply*,
                                                QAuthenticator *authenticator);
    void sslErrors(QNetworkReply*,const QList<QSslError> &errors);
    void onReplyError(QNetworkReply::NetworkError code);
    void onFinished(QNetworkReply *reply);

    void onDownloadFile(const QString &camId, const QString &preLastIndex = "");

signals:
    void hasImageDownloded(bool isDownloded, QString pathToImg);
    void emitShutDown();

private:
    QString mUser;
    QString mPass;
    QString mHost;
    QString mRequest;
    QString mPathToSave;
    QString mCamId;

    QNetworkAccessManager *mpNetManager;

    QFile *mFile;
};

#endif // HTTPS_DOWNLOADER_H
